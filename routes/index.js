
/*
 * GET home page.
 */
var formidable = require('formidable');
var bcrypt = require('bcrypt-nodejs');
var url = require('url');
var fs = require('fs');
var util = require('util');

exports.index = function(req, res){
  console.log(req.session.user)
  if(req.session.user !== undefined){
    res.render('index', { title: 'CSI CEM' });
  } else {            
    res.render('login', { title: 'CSI CEM', msg: 'Escribe tu nombre de usuario y contraseña para entrar' });
  }
};

exports.login = function (req, res) {
  if(req.body.user === 'JJVazquez') {
    var hash = "$2a$10$crMJw.5f/vujhy21JA3C3eVNrgo2KmYcq7aIm5uutlFftzXK0GkC2"
    if (bcrypt.compareSync(req.body.password, hash)){
      req.session.user = 'JJVazquez'
      console.log(req.session.user)
      res.redirect('/')
    }else{
      res.render('login', { title: 'CSI CEM', msg: 'Contraseña incorrecta' });
    }
  } else {
    res.render('login', { title: 'CSI CEM', msg: 'Nombre de usuario incorrecto' });
  }
}

exports.about = function (req, res){
  if(req.session.user !== undefined){
    res.render('about', { title: 'CSI CEM' });
  } else {            
    res.render('login', { title: 'CSI CEM', msg: 'Escribe tu nombre de usuario y contraseña para entrar' });
  }
}

exports.contact = function(req, res) {
  if(req.session.user !== undefined){
    res.render('contact', { title: 'CSI CEM' });
  } else {            
    res.render('login', { title: 'CSI CEM', msg: 'Escribe tu nombre de usuario y contraseña para entrar' });
  }
}

exports.tareas = function(req, res) {
  if(req.session.user !== undefined){
    var parsedJSON = require('../hw/stored.json');
    res.render('tareas', { title: 'CSI CEM', HW: parsedJSON });
  } else {            
    res.render('login', { title: 'CSI CEM', msg: 'Escribe tu nombre de usuario y contraseña para entrar' });
  }
}

exports.tareaNo = function(req, res) {
  if(req.session.user !== undefined){
    var number= req.params.tarea.match(/\d/)[0]
    var parsedJSON = require('../hw/stored.json');
    var content = parsedJSON[number]
    res.render('rendTarea', { title: 'CSI CEM', content: content, number: number });
  } else {            
    res.render('login', { title: 'CSI CEM', msg: 'Escribe tu nombre de usuario y contraseña para entrar' });
  }
}

exports.comment = function (req, res) {
  var form = new formidable.IncomingForm({ uploadDir: __dirname + '/../public/uploads' });
    form.keepExtensions = true;
    form.on('file', function(field, file) {
            //rename the incoming file to the file's name
                fs.rename(file.path, form.uploadDir + "/" + file.name);
        })
    form.parse(req, function(err, fields, files) {
      var parsedJSON = require('../hw/stored.json');
      parsedJSON[fields["number"]]["CommFiles"].push(files["upload"]["name"])
      parsedJSON[fields["number"]]["CommFiles"] = parsedJSON[fields["number"]]["CommFiles"].filter(function(n){return n})
      if(fields["comment"] !== ""){
        parsedJSON[fields["number"]]["Comments"].push({"user" : req.session.user, "comment" : fields["comment"]})
      }
      console.log(parsedJSON)
//      var contents = file.writeFileSync("./write_file.txt", "Text to Write");

      fs.writeFile(__dirname + "/../hw/stored.json", JSON.stringify(parsedJSON, null, 4), function(err) {
        if(err) {
          console.log(err);
        } else {
          console.log("The file was saved!");
          console.log(util.inspect({fields: fields, files: files}));          
          res.redirect("/Tareas/Tarea"+fields["number"]) 
        }
      });            
    });
}

exports.mail = function (req, res) {

  var mail = require("nodemailer").mail;

  mail({
      from: req.body.nombre + " <" + req.body.email_2 + ">",
      to: "eddy.sheikan@hotmail.com", 
      subject: "Message from CSI CEM",
      text: req.body.texto
  });
  
  res.redirect("/")
}

exports.admin = function (req, res) {
  res.render('admin', { title: 'CSI CEM' });
}

exports.crearAct = function(req, res) {
  res.render('nuevaActividad', { title: 'CSI CEM' });
}

exports.crear = function (req, res) {
  var form = new formidable.IncomingForm({ uploadDir: __dirname + '/../public/uploads' });
    form.keepExtensions = true;
    form.on('file', function(field, file) {
            //rename the incoming file to the file's name
                fs.rename(file.path, form.uploadDir + "/" + file.name);
        })
    form.parse(req, function(err, fields, files) {
      var parsedJSON = require('../hw/stored.json');
      parsedJSON[fields["nombre"]] = {"Desc" : fields["desc"],
                                      "Text" : fields["texto"],
                                      "AttFiles" : [files["upload"]["name"]].filter(function(n){return n}),
                                      "Comments" : [],
                                      "CommFiles" : []}

      console.log(parsedJSON)

      fs.writeFile(__dirname + "/../hw/stored.json", JSON.stringify(parsedJSON, null, 4), function(err) {
        if(err) {
          console.log(err);
        } else {
          console.log("The file was saved!");
          console.log(util.inspect({fields: fields, files: files}));          
          res.redirect("/Admin") 
        }
      });            
    });
}

exports.modAct = function(req, res) {
}
