
/**
 * Module dependencies.
 */

var express = require('express'),
    routes = require('./routes'),
    user = require('./routes/user'),
    http = require('http'),
    path = require('path'),
    expressLayouts = require('express-ejs-layouts');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
  app.use(express.favicon(__dirname + '/public/images/favicon.ico'));
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(expressLayouts);
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/About', routes.about);
app.get('/Contact', routes.contact);
app.get('/Tareas', routes.tareas);
app.get('/Tareas/:tarea?', routes.tareaNo);
app.get('/Admin', routes.admin);
app.get('/Admin/CrearActividad', routes.crearAct);
app.get('/users', user.list);
app.post('/login', routes.login)
app.post('/Mail', routes.mail)
app.post('/Comment', routes.comment)
app.post('/Admin/Crear', routes.crear)

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
